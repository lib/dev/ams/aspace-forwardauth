FROM denoland/deno:alpine AS main

WORKDIR /opt/aspace-forwardauth
COPY * .
RUN deno cache --lock=deno.lock main.ts

CMD ["deno", "run", "--lock=deno.lock", "--cached-only", "--allow-env", "--allow-net", "main.ts"]

HEALTHCHECK CMD ["deno", "run", "--allow-net", "--allow-env", "healthcheck.ts"]


FROM main AS smoke-test

RUN <<EOF
# Healthcheck should fail before starting the service
! deno run --allow-net --allow-env healthcheck.ts 2>/dev/null || exit 2

export ASPACE_FORWARDAUTH_ASPACE_API_URL=http://example.com:9999
export ASPACE_FORWARDAUTH_LISTEN_PORT=1234
deno run --lock=deno.lock --cached-only --allow-env --allow-net main.ts &

# Wait for server to start listening
timeout 10 sh -c 'while ! nc -z 0.0.0.0 1234; do sleep 0.1; done'

# Healthcheck should succeed
deno run --allow-net --allow-env healthcheck.ts || exit 3
EOF
