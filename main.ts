import { StatusCodes } from "http-status-codes";
import { assert } from "@std/assert";

import {
  configureLogging,
  errorAsJson,
  logger,
  logRequestResponseMiddleware,
} from "./logging.ts";
import { EnvVar, Header, SESSION_LIFETIME } from "./constants.ts";

// https://doc.traefik.io/traefik/middlewares/http/forwardauth/
// https://github.com/traefik/traefik/blob/master/pkg/middlewares/auth/forward.go

type SessionStatus = { active: boolean; cached: boolean };

class ArchivesSpaceAuth {
  private maxKnownSessions = 2048;
  private readonly knownSessions: Map<string, number>;
  private readonly currentUserUrl: string;
  constructor(readonly endpoint: string) {
    this.knownSessions = new Map();
    this.currentUserUrl = new URL("/users/current-user", endpoint).toString();
  }

  private addKnownSession(sessionId: string): void {
    while (this.knownSessions.size >= this.maxKnownSessions) {
      for (const id of this.knownSessions.keys()) {
        this.knownSessions.delete(id);
        break;
      }
    }
    this.knownSessions.set(sessionId, Date.now() + SESSION_LIFETIME);
  }

  async getSessionStatus(sessionId: string): Promise<SessionStatus> {
    const isUnexpired = (this.knownSessions.get(sessionId) ?? 0) > Date.now();
    if (isUnexpired) return { active: true, cached: true };

    const isActive = await this.isSessionActive(sessionId);
    if (isActive) this.addKnownSession(sessionId);
    return { active: isActive, cached: false };
  }

  private async isSessionActive(sessionId: string): Promise<boolean> {
    const response = await fetch(this.currentUserUrl, {
      headers: { [Header.archivesspaceSession]: sessionId },
    });
    return response.ok;
  }
}

function parseLoginPath(url: URL): { username: string } | undefined {
  // ArchivesSpace API login path is /users/:user_name/login
  const match = /^\/users\/([^/]+)\/login$/.exec(url.pathname);
  if (!match) return undefined;
  const username = match[1];
  assert(username);
  return { username };
}

export type EnvGet = Pick<Deno.Env, "get">;
export type Config = {
  aspaceApiUrl: string;
  listenHostname: string | undefined;
  listenPort: number | undefined;
};

export function loadConfig(env: EnvGet = Deno.env): Config {
  const rawUrl = env.get(EnvVar.apiUrl);
  if (!rawUrl) throw new Error(`${EnvVar.apiUrl} is not set`);
  try {
    const url = new URL(rawUrl);
    if (!/^https?:$/.test(url.protocol)) throw new Error("not an HTTP URL");
    if (url.pathname !== "/" || url.search || url.hash) {
      throw new Error("URL cannot specify a path, query or hash");
    }
  } catch (e) {
    throw new Error(
      `${EnvVar.apiUrl} is not set to a valid URL: ${e}`,
    );
  }
  const hostname = env.get(EnvVar.listenHostname) || undefined;
  const rawPort = env.get(EnvVar.listenPort) || undefined;

  let port: number | undefined = undefined;
  if (rawPort !== undefined) {
    port = Number.parseInt(rawPort);
    if (`${port}` !== rawPort || port < 0) {
      throw new Error(`${EnvVar.listenPort} is invalid: ${rawPort}`);
    }
  }

  return { aspaceApiUrl: rawUrl, listenHostname: hostname, listenPort: port };
}

function requestAuthorised(): Response {
  return new Response(null, { status: StatusCodes.NO_CONTENT });
}

function requestUnauthorised(
  { hasSession }: { hasSession: boolean },
): Response {
  const prefix = hasSession
    ? `${Header.archivesspaceSession} header has expired`
    : `${Header.archivesspaceSession} header not set`;
  return new Response(`${prefix} — Log in at /users/:user_name/login`, {
    status: StatusCodes.UNAUTHORIZED,
  });
}

type Result<T, E> = SuccessResult<T> | ErrorResult<E>;
type SuccessResult<T> = { success: true; data: T };
type ErrorResult<E> = { success: false; error: E };

type ForwardAuthRequest = { forwardedUrl: URL };
type ForwardAuthError = {
  message: string;
  errorResponse: Response;
  details?: Record<string, unknown>;
};

function errorResponse(
  { message, status, details }: {
    message: string;
    status: number;
    details?: Record<string, unknown>;
  },
): ErrorResult<ForwardAuthError> {
  return {
    success: false,
    error: {
      message,
      errorResponse: new Response(message, { status }),
      details,
    },
  };
}

function parseForwardAuthRequest(
  request: Request,
): Result<ForwardAuthRequest, ForwardAuthError> {
  if (request.method !== "GET") {
    return errorResponse({
      message: "HTTP method must be GET",
      status: StatusCodes.METHOD_NOT_ALLOWED,
    });
  }
  const url = new URL(request.url);
  if (url.pathname !== "/") {
    return errorResponse({
      message: "Path must be /",
      status: StatusCodes.NOT_FOUND,
    });
  }

  const rawForwardedPathQuery = request.headers.get(Header.forwardedPathQuery);
  if (!rawForwardedPathQuery) {
    return errorResponse({
      message: `Request must have a ${Header.forwardedPathQuery} header`,
      status: StatusCodes.BAD_REQUEST,
    });
  }
  let forwardedUrl: URL;
  try {
    forwardedUrl = new URL(rawForwardedPathQuery, "http://notused");
  } catch (_) {
    return errorResponse({
      message:
        `${Header.forwardedPathQuery} header is not a valid relative URL`,
      status: StatusCodes.BAD_REQUEST,
      details: { rawForwardedPathQuery },
    });
  }
  return { success: true, data: { forwardedUrl } };
}

function handleHealthcheckRequest(request: Request): Response | undefined {
  if (new URL(request.url).pathname === "/.well-known/x-healthcheck") {
    logger().debug("Healthcheck");
    return new Response(null, { status: StatusCodes.NO_CONTENT });
  }
}

export function serve(config: Config): Deno.HttpServer<Deno.NetAddr> {
  const aspaceAuth = new ArchivesSpaceAuth(config.aspaceApiUrl);

  const handler: Deno.ServeHandler = async (request): Promise<Response> => {
    const healthcheckResponse = handleHealthcheckRequest(request);
    if (healthcheckResponse) return healthcheckResponse;

    const result = parseForwardAuthRequest(request);
    if (!result.success) {
      logger().error(
        `Invalid ForwardAuth request: ${result.error.message}`,
        result.error.details,
      );
      return result.error.errorResponse;
    }

    const login = parseLoginPath(result.data.forwardedUrl);

    // Always permit access to the login path so that users can log in
    if (login) {
      logger().info("Granted: login URL");
      return requestAuthorised();
    }

    // Requests to non-login URLs are only allowed with a valid session
    const sessionId = request.headers.get(Header.archivesspaceSession);
    if (!sessionId) {
      logger().info("Rejected: non-login URL with no session");
      return requestUnauthorised({ hasSession: false });
    }

    let sessionStatus: SessionStatus;
    try {
      sessionStatus = await aspaceAuth.getSessionStatus(sessionId);
    } catch (e) {
      logger().error(`Failed to validate sessionId with API`, {
        error: errorAsJson(e),
      });
      return new Response("Failed to verify sessionId", {
        status: StatusCodes.BAD_GATEWAY,
      });
    }

    if (sessionStatus.active) {
      logger().info(`Granted: active session`, {
        cached: sessionStatus.cached,
      });
      return requestAuthorised();
    } else {
      logger().info("Rejected: expired session");
      return requestUnauthorised({ hasSession: true });
    }
  };

  return Deno.serve({
    hostname: config.listenHostname,
    port: config.listenPort,
    onListen(localAddr) {
      logger().info(
        `Listening on http://${localAddr.hostname}:${localAddr.port}`,
      );
    },
  }, logRequestResponseMiddleware(handler));
}

export async function main() {
  configureLogging();

  let config: Config;
  try {
    config = loadConfig();
  } catch (e) {
    logger().error(`Failed to load config: ${e}`);
    Deno.exit(1);
  }
  await serve(config).finished;
}

if (import.meta.main) {
  main();
}
