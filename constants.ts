export enum Header {
  archivesspaceSession = "X-ArchivesSpace-Session",
  /** The URL path & query for the original request's URL, e.g. `/foo?a=1` */
  forwardedPathQuery = "X-Forwarded-Uri",
}

export enum EnvVar {
  apiUrl = "ASPACE_FORWARDAUTH_ASPACE_API_URL",
  listenPort = "ASPACE_FORWARDAUTH_LISTEN_PORT",
  listenHostname = "ASPACE_FORWARDAUTH_LISTEN_HOSTNAME",
  logLevel = "ASPACE_FORWARDAUTH_LOG_LEVEL",
}

export const SESSION_LIFETIME = 1000 * 60 * 60;
