import { assert, assertEquals, assertThrows } from "@std/assert";
import { assertSpyCall, stub } from "@std/testing/mock";
import { FakeTime } from "@std/testing/time";
import { EnvGet, loadConfig, serve } from "./main.ts";
import { Header, SESSION_LIFETIME } from "./constants.ts";
import { StatusCodes } from "http-status-codes";

function env(vars: Record<string, string>): EnvGet {
  return new Map(Object.entries(vars));
}

Deno.test("loadConfig", async (t) => {
  await t.step("loads valid config", () => {
    const config = loadConfig(env({
      ASPACE_FORWARDAUTH_ASPACE_API_URL: "http://localhost:1234",
      ASPACE_FORWARDAUTH_LISTEN_HOSTNAME: "0.0.0.0",
      ASPACE_FORWARDAUTH_LISTEN_PORT: "4321",
    }));

    assertEquals(config, {
      aspaceApiUrl: "http://localhost:1234",
      listenHostname: "0.0.0.0",
      listenPort: 4321,
    });
  });

  await t.step("loads valid config without listen address", () => {
    const config = loadConfig(env({
      ASPACE_FORWARDAUTH_ASPACE_API_URL: "http://localhost:1234",
    }));

    assertEquals(config, {
      aspaceApiUrl: "http://localhost:1234",
      listenHostname: undefined,
      listenPort: undefined,
    });
  });

  await t.step("API URL is required", () => {
    assertThrows(
      () =>
        loadConfig(
          env({}),
        ),
      Error,
      "ASPACE_FORWARDAUTH_ASPACE_API_URL is not set",
    );
  });

  await t.step("rejects invalid URL", () => {
    assertThrows(
      () =>
        loadConfig(
          env({
            ASPACE_FORWARDAUTH_ASPACE_API_URL: "ftp://localhost:1234",
          }),
        ),
      Error,
      "ASPACE_FORWARDAUTH_ASPACE_API_URL is not set to a valid URL",
    );
  });

  await t.step("rejects invalid port", () => {
    assertThrows(
      () =>
        loadConfig(
          env({
            ASPACE_FORWARDAUTH_ASPACE_API_URL: "http://localhost:1234",
            ASPACE_FORWARDAUTH_LISTEN_PORT: "-1024",
          }),
        ),
      Error,
      "ASPACE_FORWARDAUTH_LISTEN_PORT is invalid: -1024",
    );
  });
});

const API_URL = "http://api.aspace:1234";

function stubApiUrlFetch(response: Response) {
  const originalFetch = fetch;
  return stub(globalThis, "fetch", (input, init) => {
    const req = new Request(input, init);
    const url = new URL(req.url);
    if (url.origin === API_URL) {
      return Promise.resolve(response);
    }
    return originalFetch(req);
  });
}

Deno.test("serve() ", async (t) => {
  await using server = serve({
    aspaceApiUrl: API_URL,
    listenHostname: "localhost",
    listenPort: 0,
  });
  const sessionId = "1234";
  const serverUrl = new URL(`http://localhost:${server.addr.port}/`);
  using time = new FakeTime();

  /** Make a request to the ForwardAuth server to check access.
   *
   * The request mimics those made by the Traefik ForwardAuth middleware.
   */
  const makeAuthRequest = (
    apiUrl: URL,
    options: { headers?: Record<string, string> } = {},
  ): Promise<Response> => {
    // The real ForwardAuth middleware includes more headers, but we only care
    // about the forwarded URL path. It always makes GET requests.
    return fetch(new URL("/", serverUrl), {
      headers: {
        [Header.forwardedPathQuery]: `${apiUrl.pathname}${apiUrl.search}`,
        ...options.headers,
      },
    });
  };

  await t.step(
    "Requests to login URL are allowed unconditionally",
    async () => {
      const resp = await makeAuthRequest(
        new URL("/users/example/login", API_URL),
      );
      assert(resp.ok);
      await resp.text(); // close the response
    },
  );

  await t.step("Non-standard ForwardAuth requests are rejected", async (t) => {
    await t.step("Method must be GET", async () => {
      const resp = await fetch(serverUrl, { method: "POST" });
      await resp.body?.cancel();
      assertEquals(resp.status, StatusCodes.METHOD_NOT_ALLOWED);
    });

    await t.step("Path must be /", async () => {
      const resp = await fetch(new URL("/foo", serverUrl));
      await resp.body?.cancel();
      assertEquals(resp.status, StatusCodes.NOT_FOUND);
    });

    await t.step("Forwarded URL must be provided", async () => {
      const resp = await fetch(serverUrl);
      const text = await resp.text();
      assertEquals(resp.status, StatusCodes.BAD_REQUEST);
      assertEquals(text, "Request must have a X-Forwarded-Uri header");
    });

    await t.step("Forwarded URL must be a valid URL", async () => {
      const resp = await fetch(serverUrl, {
        // In practice it's quite hard to form a URL path and query that won't
        // parse, not sure if there are any cases that throw.
        // Here we use an invalid URL, but in practice the value provided by
        // Traefik is a path starting with /
        headers: { [Header.forwardedPathQuery]: "http://1.2.3.400" },
      });
      const text = await resp.text();
      assertEquals(resp.status, StatusCodes.BAD_REQUEST);
      assertEquals(text, "X-Forwarded-Uri header is not a valid relative URL");
    });
  });

  await t.step("Session IDs are validated", async (t) => {
    await t.step("Uncached requests are checked against API", async () => {
      using stubFetch = stubApiUrlFetch(new Response(null, { status: 200 }));

      const resp = await makeAuthRequest(new URL("/repositories", serverUrl), {
        headers: { [Header.archivesspaceSession]: sessionId },
      });
      await resp.text(); // close the response

      assert(resp.ok); // Request is authorised

      assertEquals(stubFetch.calls.length, 2);
      // API was called to check the session
      assertSpyCall(stubFetch, 1, {
        args: [`${API_URL}/users/current-user`, {
          headers: { [Header.archivesspaceSession]: sessionId },
        }],
      });
    });

    await t.step("Cached requests are not re-checked against API", async () => {
      using stubFetch = stubApiUrlFetch(new Response(null, { status: 200 }));

      const resp = await makeAuthRequest(new URL("/repositories", serverUrl), {
        headers: { [Header.archivesspaceSession]: sessionId },
      });
      await resp.text(); // close the response

      assert(resp.ok); // Request is authorised
      assertEquals(stubFetch.calls.length, 1); // API not called
    });

    await t.step("Expired requests are re-checked against API", async () => {
      // Cached session has now expired
      time.tick(SESSION_LIFETIME + 1);

      using stubFetch = stubApiUrlFetch(
        new Response(null, { status: StatusCodes.UNAUTHORIZED }),
      );

      const resp = await makeAuthRequest(new URL("/repositories", serverUrl), {
        headers: { [Header.archivesspaceSession]: sessionId },
      });
      await resp.text(); // close the response

      // Session is no longer authorized
      assertEquals(resp.status, StatusCodes.UNAUTHORIZED);

      // API was called to check the session
      assertEquals(stubFetch.calls.length, 2);
      assertSpyCall(stubFetch, 1, {
        args: [`${API_URL}/users/current-user`, {
          headers: { [Header.archivesspaceSession]: sessionId },
        }],
      });
    });
  });

  await t.step("Healthcheck requests", async () => {
    const resp = await fetch(new URL("/.well-known/x-healthcheck", serverUrl));
    await resp.text(); // close the response

    assert(resp.ok); // Request is authorised
  });
});
