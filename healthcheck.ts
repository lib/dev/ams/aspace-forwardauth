import { EnvVar } from "./constants.ts";

const hostname = Deno.env.get(EnvVar.listenHostname) || "localhost";
const port = Number.parseInt(Deno.env.get(EnvVar.listenPort) || "") || 8000;
const resp = await fetch(
  `http://${hostname}:${port}/.well-known/x-healthcheck`,
);
const text = await resp.text();

if (!resp.ok) {
  console.error(
    `Unhealthy: service responded to always-allowed login URL request with ${resp.status}: ${
      JSON.stringify(text)
    }`,
  );
  Deno.exit(1);
}
