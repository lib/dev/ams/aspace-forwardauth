IMAGE_VERSION = "1"
RELEASE_BRANCH = "main"

variable "CI_COMMIT_BRANCH" {
    default = ""
}

variable "CI_COMMIT_TAG" {
    default = ""
}

variable "TAG_PREFIX" {
    default = "registry.gitlab.developers.cam.ac.uk/lib/dev/ams/"
}

function "tag_date" {
    params = []
    result = formatdate("YYYY.MM.DD", timestamp())
}

function "tags" {
    params = []
    result = (
        CI_COMMIT_BRANCH == RELEASE_BRANCH
        ? ["latest", IMAGE_VERSION, "${IMAGE_VERSION}-${tag_date()}"]
        : (
            CI_COMMIT_BRANCH != ""
            ? ["branch-${CI_COMMIT_BRANCH}", "branch-${CI_COMMIT_BRANCH}-${tag_date()}"]
            : ["local"]
        )
    )
}

group default {
    targets = ["tasks", "smoke-test", "image"]
}

target image {
    tags = formatlist("${TAG_PREFIX}aspace-forwardauth:%s", tags())
    target = "main"
    labels = {
        "org.opencontainers.image.title" = "aspace-forwardauth"
        "org.opencontainers.image.description" = "A Traefik ForwardAuth authentication server to limit access to ArchivesSpace's API"
        "org.opencontainers.image.url" = "https://gitlab.developers.cam.ac.uk/lib/dev/ams/aspace-forwardauth"
    }
}

target smoke-test {
    extends = ["image"]
    target = "smoke-test"
    no-cache-filter = ["smoke-test"]
}

target "tasks" {
    name = "task-${task}"
    dockerfile = "Dockerfile.tasks"
    matrix = {
        task = ["lint", "fmt", "test"]
    }
    target = task
    no-cache-filter = ["test"]  # don't cache test results — always re-run tests
    output = ["type=cacheonly"]
}
