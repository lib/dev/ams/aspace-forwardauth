import * as log from "@std/log";
import { EnvVar } from "./constants.ts";

export function logger() {
  return log.getLogger("aspace-forwardauth");
}

const DEFAULT_LOG_LEVEL: log.LevelName = "INFO";

function parseLogLevelFromEnvironment(): {
  level: log.LevelName;
  error?: string;
} {
  const level = Deno.env.get(EnvVar.logLevel) || DEFAULT_LOG_LEVEL;
  try {
    log.getLevelByName(level as log.LevelName);
    return { level: level as log.LevelName };
  } catch (_) {
    return {
      level: DEFAULT_LOG_LEVEL,
      error:
        `Error: Invalid ${EnvVar.logLevel} value: '${level}', valid choices are: ${
          Object.keys(log.LogLevels).join(", ")
        }`,
    };
  }
}

type ErrorJson = {
  name: string | null;
  message: string | null;
  stack: string | null;
};

export function errorAsJson(error: unknown): ErrorJson {
  const e = error as Partial<Error>;
  return {
    name: e.name ?? null,
    message: e.message ?? null,
    stack: e.stack ?? null,
  };
}

export function configureLogging() {
  const { level, error } = parseLogLevelFromEnvironment();

  log.setup({
    handlers: {
      console: new log.ConsoleHandler(level, {
        formatter: log.formatters.jsonFormatter,
      }),
    },
    loggers: {
      "aspace-forwardauth": {
        level: level,
        handlers: ["console"],
      },
      "http": {
        level: level,
        handlers: ["console"],
      },
    },
  });

  if (error) logger().error(error);
}

export function logRequestResponseMiddleware(
  next: Deno.ServeHandler,
): Deno.ServeHandler {
  return async function logRequestResponse(
    request: Request,
    info: Deno.ServeHandlerInfo,
  ): Promise<Response> {
    let requestId: string | undefined;
    const lg = log.getLogger("http");

    if (lg.level <= log.LogLevels.DEBUG) {
      requestId = crypto.randomUUID();
      lg.debug(`Request ${request.method} ${request.url}`, {
        requestId,
        headers: [...request.headers.entries()],
      });
    }

    let response: Response;
    try {
      response = await next(request, info);
    } catch (e) {
      lg.error("Failed to handle request", {
        requestId,
        error: errorAsJson(e),
      });
      throw e;
    }

    if (lg.level <= log.LogLevels.DEBUG) {
      lg.debug("Response", {
        requestId,
        status: response.status,
        headers: [...response.headers.entries()],
      });
    }

    return response;
  };
}
