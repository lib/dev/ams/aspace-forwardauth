# ArchivesSpace Traefik ForwardAuth

This allows for the ArchivesSpace API to be restricted to authenticated users.
(Typically the ArchivesSpace API is exposed to specific IP addresses.)

It's a [Traefik ForwardAuth] HTTP service. With ArchivesSpace deployed behind a
Traefik load balancer, Traefik can limit access to the ArchivesSpace API by
configuring ForwardAuth middleware delegating to this service for access-control
decisions.

This service checks the `X-ArchivesSpace-Session` header of incoming requests,
and only allows them to proceed if they have a valid session. It checks for
valid sessions by making its own requests to the ArchivesSpace API, and then
caches known-good session IDs in memory for some time.

It probably also works with other similar middleware, like
[nginx's auth_request] (but I've not tested).

[Traefik ForwardAuth]: https://doc.traefik.io/traefik/middlewares/http/forwardauth/
[nginx's auth_request]: https://docs.nginx.com/nginx/admin-guide/security-controls/configuring-subrequest-authentication/

## Deploy

A container image is published in this
[repository's registry](https://gitlab.developers.cam.ac.uk/lib/dev/ams/aspace-forwardauth/container_registry/).
It's rebuilt daily against upstream images, but there are daily tags too which
won't change after the day they're built.

Run the image manually like this:

```console
$ docker container run --rm -it \
    -e ASPACE_FORWARDAUTH_ASPACE_API_URL=http://localhost:8089 \
    -e ASPACE_FORWARDAUTH_LOG_LEVEL=DEBUG \
    -p 8000:8000 \
    registry.gitlab.developers.cam.ac.uk/lib/dev/ams/aspace-forwardauth
{"level":"INFO","datetime":1717674240851,"message":"Listening on http://localhost:8000"}
```

In reality it needs to be run alongside Traefik and an
[ArchivesSpace backend container](), something like this:

```yaml
services:
  api-auth:
    environment:
      ASPACE_FORWARDAUTH_ASPACE_API_URL: http://archivesspace-backend:8089
    image: registry.gitlab.developers.cam.ac.uk/lib/dev/ams/aspace-forwardauth:branch-main
    networks:
      - backend
      - traefik-public
  archivesspace-backend:
    image: registry.gitlab.developers.cam.ac.uk/lib/dev/ams/archivesspace-container/archivesspace
    command:
      - run-server
      - backend
    deploy:
      labels:
        traefik.docker.network: traefik-public
        traefik.enable: "true"
        traefik.http.routers.archivesspace-backend-https.entrypoints: https
        traefik.http.routers.archivesspace-backend-https.rule: Host(`api.archivesspace.example.com`)
        traefik.http.routers.archivesspace-backend-https.tls: "true"
        traefik.http.routers.archivesspace-backend-https.middlewares: api-auth
        traefik.http.services.archivesspace-backend.loadbalancer.server.port: "8089"
        traefik.http.middlewares.api-auth.forwardauth.address: http://ams_api-auth:8000/
    # ...
```

### Configuration Environment Variables

Configuration is done using environment variables:

- `ASPACE_FORWARDAUTH_ASPACE_API_URL` — the URL of the ArchivesSpace API (from
  the point of view of the container, not necessarily the public URL).
- `ASPACE_FORWARDAUTH_LOG_LEVEL` — The log level to log at. Default is `INFO`,
  which reports details of how each request is handled. `DEBUG` also logs raw
  HTTP requests. `ERROR` only logs errors.
- `ASPACE_FORWARDAUTH_LISTEN_PORT` — The port for the HTTP server to listen on;
  default is `8000`.
- `ASPACE_FORWARDAUTH_LISTEN_HOSTNAME` — The address for the HTTP server to
  listen on; default is `0.0.0.0` (all interfaces).
